﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");

            //* Exercise 4
            Task4 task4 = new Task4();
            Console.Write("Enter the natural number: ");
            if (task4.TryParseNaturalNumber(Console.ReadLine(), out int result))
            {
                Console.WriteLine("Fibonacci Sequence: ");
                foreach (var item in task4.GetFibonacciSequence(result))
                {
                    Console.WriteLine(item);
                }
            }
            else
            {
                Console.WriteLine("Input is not valid");
            }

            //* Exercise 5
            Task5 task5 = new Task5();
            Console.Write("Enter the number: ");
            Console.WriteLine("The reverse number: " + task5.ReverseNumber(Convert.ToInt32(Console.ReadLine())));

            //* Exercise 6
            Task6 task6 = new Task6();
            Console.Write("Enter the size of an array: ");
            int size = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Array");
            int[] array = task6.GenerateArray(size);
            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("New Array");
            foreach (var item in task6.UpdateElementToOppositeSign(array))
            {
                Console.WriteLine(item);
            }

            //* Exercise 7
            Task7 task7 = new Task7();
            Console.Write("Enter the size of an array: ");
            int size_task7 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Array");
            int[] array_task7 = task7.GenerateArray(size_task7);
            foreach (var item in array_task7)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("New Array");
            foreach (var item in task7.FindElementGreaterThenPrevious(array_task7))
            {
                Console.WriteLine(item);
            }
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            return int.TryParse(input, out result) && result >= 0;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] fibonacciSequence = new int[n];

            if (n == 0)
            {
                fibonacciSequence = new int[0];
            }
            else if (n == 1)
            {
                fibonacciSequence[0] = 0;
            }
            else
            {
                fibonacciSequence[0] = 0;
                fibonacciSequence[1] = 1;
                for (int i = 2; i < n; i++)
                {
                    fibonacciSequence[i] = fibonacciSequence[i - 1] + fibonacciSequence[i - 2];
                }
            }

            return fibonacciSequence;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string source;
            if (sourceNumber < 0)
            {
                source = sourceNumber.ToString().Split('-')[1];
            }
            else
            {
                source = sourceNumber.ToString();
            }

            StringBuilder reverseNumber = new StringBuilder();

            for (int i = source.Length - 1; i >= 0; i--)
            {
                reverseNumber.Append(source[i]);
            }

            return sourceNumber < 0 ? Convert.ToInt32("-" + reverseNumber) : Convert.ToInt32(reverseNumber);
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] source;
            if (size <= 0)
            {
                source = new int[0];
            }
            else
            {
                source = new int[size];
                Random random = new Random();
                for (int i = 0; i < size; i++)
                {
                    source[i] = random.Next(-50, 50);
                }
            }

            return source;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = -source[i];
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] source;
            if (size <= 0)
            {
                source = new int[0];
            }
            else
            {
                source = new int[size];
                Random random = new Random();
                for (int i = 0; i < size; i++)
                {
                    source[i] = random.Next(-50, 50);
                }
            }

            return source;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> greaterThenPrevious = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                    greaterThenPrevious.Add(source[i]);
            }

            return greaterThenPrevious;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            throw new NotImplementedException();
        }
    }
}
